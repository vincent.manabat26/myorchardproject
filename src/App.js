import React, { Component } from "react";
import "./App.css";
import Component01 from "./components/component01";
import Component02 from "./components/component02";
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      details: [],
    };
  }

  handleDetails = (data) => {
    this.setState({
      details: data,
    });
  };

  render() {
    return (
      <div className="App">
        <div className="App-header">
          <Component01 />
          <Component02 handleDetails={this.handleDetails.bind(this)} />
        </div>
      </div>
    );
  }
}

export default App;
