import React, { Component } from "react";
import Lightbox from "react-image-lightbox";
import "react-image-lightbox/style.css";

class displayImage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      photoIndex: 0,
      isOpen: false,
      newImage: [],
    };
  }

  componentDidMount() {
    const { gallery, ImageOpen } = this.props;
    this.setState({ newImage: gallery, isOpen: ImageOpen });
  }

  handleClose() {
    this.setState({ isOpen: false });
    this.props.ImageClosed(false);
  }

  render() {
    const { photoIndex, isOpen, newImage } = this.state;
    console.log([newImage]);
    return (
      <div>
        {isOpen && (
          <Lightbox
            mainSrc={newImage}
            nextSrc={newImage[(photoIndex + 1) % newImage.length]}
            prevSrc={
              newImage[(photoIndex + newImage.length - 1) % newImage.length]
            }
            onCloseRequest={this.handleClose.bind(this)}
            enableZoom={false}
          />
        )}
      </div>
    );
  }
}

export default displayImage;
