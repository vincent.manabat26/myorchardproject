import React, { Component } from "react";
import image01 from "../assets/component-01/Image-01@2x.jpg";
import image02 from "../assets/component-01/Image-02@2x.jpg";
import image03 from "../assets/component-01/Image-03@2x.jpg";

import DisplayImage from "./Modal/displayImage";

import "./component01.css";

class component01 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      setOpen: false,
      source: "",
    };
  }

  handleOpen(e) {
    this.setState({
      setOpen: true,
      source: e.target.src,
    });
  }

  handleClose(e) {
    this.setState({
      setOpen: e,
      source: "",
    });
  }

  render() {
    const { source } = this.state;
    return (
      <div className="row">
        <div className="column">
          <img
            src={image01}
            alt={image01}
            onClick={this.handleOpen.bind(this)}
          />
        </div>
        <div className="column">
          <img
            src={image02}
            alt={image02}
            onClick={this.handleOpen.bind(this)}
          />
          <img
            src={image03}
            alt={image03}
            onClick={this.handleOpen.bind(this)}
          />
        </div>
        <div className="column ">
          <div className="detailsContainer">
            <h4 className="mainTitle">ANSWER YOUR BODY'S NEEDS</h4>
            <p>
              The way ingredients are sourced affects the way we nourish our
              bodies. Author Mark Schatzer believes our body naturally develops
              an appetite for the foods and nutrients it needs to be healthy,
              but that artificial flavorings are getting in the way. This can be
              reversed by focusing on high-quality ingredients and being mindful
              as your appetite guides you to consume according to your body's
              needs.
            </p>
            <h5 className="subTitle">BE MINDFUL</h5>
            <p className="subMessage">
              Sourcing local or organic food is a good way to start being more
              mindful about what you're cooking and eating.
            </p>
          </div>
        </div>
        {source && (
          <DisplayImage
            gallery={this.state.source}
            ImageOpen={this.state.setOpen}
            ImageClosed={this.handleClose.bind(this)}
          />
        )}
      </div>
    );
  }
}

export default component01;
