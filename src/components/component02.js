import React, { Component } from "react";
import image01 from "../assets/component-02/Image-01.jpg";
import image02 from "../assets/component-02/Image-02.jpg";
import image03 from "../assets/component-02/Image-03.jpg";

import DisplayImage from "./Modal/displayImage";

import "./component02.css";

const itemData = [
  {
    img: image01,
    title: "Summer Lunch Menu By Mark Best",
    desc: "AEG ambassador Mark Best's summer eats are guaranteed to help you make the most of the warmer weather and entertaining at home.",
  },
  {
    img: image02,
    title: "A Traditional Christmas Eve, Mark Best Style",
    desc: "One of Australia's best chefs and AEG ambassador, Mark Best, shares his favorite Christmas Eve menu which is sure to impress your guests",
  },
  {
    img: image03,
    title: "Taking Taste Further",
    desc: "This exclusive cookbook gives you all the know-how you need. We've designed it ot make sure you get the most out of our products - and the best out of your food.",
  },
];

class component02 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      setOpen: false,
      source: "",
    };
  }

  handleOpen(e) {
    this.setState({
      setOpen: true,
      source: e.target.src,
    });
  }

  handleClose(e) {
    this.setState({
      setOpen: e,
      source: "",
    });
  }

  handleLog(e) {
    console.log(e.target.getAttribute("value"));
  }

  render() {
    const { source } = this.state;
    return (
      <div className="container">
        <h1 className="componentTitle">ALL THE LAST FROM AEG</h1>

        <div className="cardContainer">
          {itemData.map((item, key) => (
            <div className="card">
              <img
                className="imageComponent"
                src={item.img}
                alt={item.title}
                onClick={this.handleOpen.bind(this)}
                key={key}
              />
              <h5 className="title">{item.title}</h5>
              <h5 className="desc">{item.desc}</h5>
              <a href="#" onClick={this.handleLog.bind(this)}>
                <h5 className="readMore" value={item.desc}>
                  READ MORE
                </h5>
              </a>
            </div>
          ))}
        </div>
        {source && (
          <DisplayImage
            gallery={this.state.source}
            ImageOpen={this.state.setOpen}
            ImageClosed={this.handleClose.bind(this)}
          />
        )}
      </div>
    );
  }
}

export default component02;
